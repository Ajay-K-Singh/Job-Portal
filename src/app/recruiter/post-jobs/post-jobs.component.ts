import { Component, OnInit, Input, OnChanges} from '@angular/core';
import { PostJob } from '../../models/post-job.model';
import { JobPostingService } from '../../services/job-posting.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { faLocationArrow, faEdit, faSync, faBan } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-post-jobs',
  templateUrl: './post-jobs.component.html',
  styleUrls: ['./post-jobs.component.scss']
})
export class PostJobsComponent implements OnInit {
  faLocation = faLocationArrow;
  faEditIcon = faEdit;
  faSyncIcon = faSync;
  faCancelIcon = faBan;
  id: number;
  private routeParamSub: any;
  private jobPostPartial: Subscription;
  jobPost: PostJob;
  showPostJobStartFields = false;
  showUpdationFields = false;
  locationLabel = 'Location';
  locationPlaceholder = 'Location of the Job';
  nameOfCompanyLabel = 'Name Of Copmany';
  companyInfo: object;
  jobTitle: string;
  location: string;
  hasCompanyBeenUpdated: boolean;
  hasJobTitleChanged: boolean;
  hasLocationBeenUpdated: boolean;

  constructor(public jobPostingService: JobPostingService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.routeParamSub = this.route.params.subscribe(params => {
      this.id = params.id;
      this.jobPostingService.getJobPost(this.id);
      this.jobPostPartial = this.jobPostingService.getJobPostsUpdateUpdateListener().subscribe((jobPosts: PostJob[]) => {
        this.jobPost = jobPosts[0];
        this.setJobPostValues();
        this.resetUpdatedValues();
      });
   });
  }

  onChangeJobTitle(newTitle) {
    if (newTitle !== this.jobPost.jobTitle) {
      this.hasJobTitleChanged = true;
    } else { this.hasJobTitleChanged = false; }

  }

  onEditPreviousInfo() {
    this.showPostJobStartFields = true;
  }

  onSubmit() {
    if (this.hasCompanyBeenUpdated || this.hasJobTitleChanged || this.hasLocationBeenUpdated) {
      const updatedPostJobInfo = {
        id: this.jobPost.id,
        jobTitle: this.jobTitle,
        nameOfCompany: this.companyInfo,
        location: this.location
      };
      this.jobPostingService.updateJobPost(this.jobPost.id, updatedPostJobInfo);
    }
    this.showPostJobStartFields = false;
  }

  private setSelectedCompamy(event): void {
    this.companyInfo = {
      nameOfCompany: event.company.name,
      logo: event.company.logo,
      domain: event.company.domain
    };
    if (this.companyInfo['nameOfCompany'] !== this.jobPost.nameOfCompany['nameOfCompany']) {
      this.hasCompanyBeenUpdated = true;
    } else { this.hasCompanyBeenUpdated = false; }
  }

  private setSelectedLocation(event): void {
    this.location = event.location.description;
    if (this.location !== this.jobPost.location) {
      this.hasLocationBeenUpdated = true;
    } else { this.hasLocationBeenUpdated = false; }
  }

  onCancelUpdate() {
    this.setJobPostValues();
    this.showPostJobStartFields = false;
  }

  setJobPostValues() {
    this.jobTitle = this.jobPost.jobTitle;
    this.location = this.jobPost.location;
    this.companyInfo = this.jobPost.nameOfCompany;
  }

  resetUpdatedValues() {
    this.hasCompanyBeenUpdated = false;
    this.hasJobTitleChanged = false;
    this.hasLocationBeenUpdated = false;
  }
}
