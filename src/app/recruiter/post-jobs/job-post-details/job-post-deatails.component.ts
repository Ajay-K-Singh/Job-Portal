import { Component, OnInit } from '@angular/core';
import { faBan, faSync } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-job-post-details',
  templateUrl: './job-post-deatails.component.html',
  styleUrls: ['./job-post-deatails.component.scss']
})

export class JobPostDetailsComponent implements OnInit {
  faCancelIcon = faBan;
  faSyncIcon = faSync;
  jobTypes = [
    {name: 'Full-Time', value: 0},
    {name: 'Part-Time', value: 1},
    {name: 'Temporary', value: 2},
    {name: 'Contract', value: 3},
    {name: 'Internship', value: 4},
    {name: 'Commision', value: 5},
    {name: 'Volunteer', value: 6},
    {name: 'Fresher', value: 7},
    {name: 'Walk-In', value: 8}
  ];
  noOfpositions = [
    {name: '1 ', value: 0},
    {name: '> 2', value: 1},
    {name: '> 10', value: 2}
  ];
  ngOnInit() {

  }

  onSubmit() {
    console.log('Yes');
  }
}
