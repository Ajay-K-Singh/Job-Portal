import { Injectable } from '@angular/core';
import { PostJob } from '../models/post-job.model';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '../../../node_modules/@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})

export class JobPostingService {
  private jobPosts: PostJob[] = [];
  constructor(private http: HttpClient, private router: Router, private authenticationService: AuthenticationService) { }
  private jobsPosted = new Subject<PostJob[]>();
  companyInfo: object;
  locationOfJob: string;
  jobTitle: object;

  startJobPosting(jobTitle: string, nameOfCompany: object, location: string) {
    const postJob = {
      id: null,
      jobTitle: jobTitle,
      nameOfCompany,
      location: location
    };
    const requestPath = localStorage.getItem('loggedInAs');
    this.http.post<{ id: string, jobTitle: string, nameOfCOmpany: object, experienceFrom: number,
      experienceTo: number, location: string, keySkills: object, jobDescription: string,
      salaryFrom: number, salaryTo: number}>(`https://localhost:3000/api/${requestPath}/post-job`, postJob)
      .subscribe((response) => {
        const id = (<any>response).jobPost._id;
        postJob.id = id;
        this.jobPosts.push(postJob);
        this.jobsPosted.next([...this.jobPosts]);
        this.router.navigate([`/recruiter/job-post/${postJob.id}`]);
      });
  }

  getJobPost(jobPostId) {
    const requestPath = localStorage.getItem('loggedInAs');
    this.http.get<{ jobPosts: any }>(`https://localhost:3000/api/${requestPath}/job-post`, {
      params: {
        id: jobPostId
      }})
      .pipe(map((response) => {
        return response.jobPosts.map((res) => {
          return {
            id: res._id,
            jobTitle: res.jobTitle,
            location: res.location,
            nameOfCompany: res.nameOfCompany,
           };
        });
      }))
      .subscribe((jobPosts) => {
        this.jobPosts = jobPosts;
        this.jobsPosted.next([...this.jobPosts]);
      });
  }

  updateJobPost(jobPostId, updatedPostJobInfo: PostJob) {
    const array = [];
    array.push(updatedPostJobInfo);
    const requestPath = localStorage.getItem('loggedInAs');
      this.http.put<{ id: string, jobTitle: string, nameOfCOmpany: object }>
      (`https://localhost:3000/api/${requestPath}/post-job/${jobPostId}`, updatedPostJobInfo)
      .subscribe((response) => {
        this.jobPosts = array;
        this.jobsPosted.next([...this.jobPosts]);
      });
  }

  getJobPosts() {
    const requestPath = localStorage.getItem('loggedInAs');
    this.http.get<{ jobPosts: any }>(`https://localhost:3000/api/${requestPath}/job-posts`)
      .pipe(map((response) => {
        return response.jobPosts.map((res) => {
          return {
            id: res._id,
            jobTitle: res.jobTitle,
            location: res.location,
            keySkills: res.keySkills,
            nameOfCompany: res.nameOfCompany,
            experienceFrom: res.experienceFrom,
            experienceTo: res.experienceTo,
            jobDescription: res.jobDescription,
            salaryFrom: res.salaryFrom,
            salaryTo: res.salaryTo
          };
        });
      }))
      .subscribe((jobPosts) => {
        this.jobPosts = jobPosts;
        this.jobsPosted.next([...this.jobPosts]);
      });
  }

  getJobPostsUpdateUpdateListener() {
    return this.jobsPosted.asObservable();
  }

  deleteJobPost(jobPostId: string) {
    this.http.delete('https://localhost:3000/api/recruiter/job-posts/' + jobPostId)
      .subscribe(() => {
        const updatedJobPosts = this.jobPosts.filter(jobPost => jobPost.id !== jobPostId);
        this.jobPosts = updatedJobPosts;
        this.jobsPosted.next([...this.jobPosts]);
      });
  }
}
