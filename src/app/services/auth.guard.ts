import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class AuthenticationGuard implements CanActivate {
  isAuthenticated = false;
  constructor(private authenticationService: AuthenticationService, private router: Router) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    const isSessionAuthenticated = this.checkSessionAuthentication();
    const isAuthenticated = this.checkRouteAuthorization() && isSessionAuthenticated;
    return isAuthenticated;
  }

  checkRouteAuthorization() {
    const currentRoute = this.router.url.split('/')[1];
      if (this.router.url.split('/')[1] !== localStorage.getItem('loggedInAs')
      && currentRoute !== '/' && currentRoute !== '' && currentRoute !== '#_=_') {
      window.location.replace('/unauthorized');
      return false;
    }
    return true;
  }

  checkSessionAuthentication() {
    const isSessionAuthenticated = localStorage.getItem('isAuthenticatedSession') === 'true';
    if (isSessionAuthenticated) {
      return true;
    }
    this.router.navigate(['/unauthorized']);
    return false;
  }
}
