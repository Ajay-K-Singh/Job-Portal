import { Component, OnInit } from '@angular/core';
import { JobPostingService } from '../../services/job-posting.service';
import { PostJob } from '../../models/post-job.model';
import { Subscription } from 'rxjs';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-jobseeker-listings',
  templateUrl: './jobseeker-listings.component.html',
  styleUrls: ['./jobseeker-listings.component.scss']
})
export class JobSeekerListingsComponent implements OnInit {
  faSearch = faSearch;
  onShowSearchFilters = false;
  jobPosts: PostJob[] = [];
  private jobListings: Subscription;
  constructor(public jobPostingService: JobPostingService){}
  
  ngOnInit() {
    this.jobPostingService.getJobPosts();
    this.jobListings = this.jobPostingService.getJobPostsUpdateUpdateListener().subscribe((jobPosts: PostJob[]) => {
      this.jobPosts = jobPosts;
    });
  }

  onSearch() {
    this.onShowSearchFilters = true;
  }

  onCancelSearch() {
    this.onShowSearchFilters = false;
  }


  ngOnDestroy() {
    if(this.jobListings){
      this.jobListings.unsubscribe();
     }
  }
}
