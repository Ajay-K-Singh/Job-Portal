import {coerceNumberProperty} from '@angular/cdk/coercion';
import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-mat-slider',
  templateUrl: './material-slider.component.html',
  styleUrls: ['./material-slider.component.scss'],
})
// tslint:disable-next-line:component-class-suffix
export class AppMaterialSlider {
  @Input() autoTicks = false;
  @Input() disabled = false;
  @Input() invert = false;
  @Input() max = 100;
  @Input() min = 0;
  @Input() showTicks = false;
  @Input() step = 1;
  @Input() thumbLabel = false;
  @Input() value = 0;
  @Input() vertical = false;
  @Input() suffix: string;

  get tickInterval(): number | 'auto' {
    return this.showTicks ? (this.autoTicks ? 'auto' : this._tickInterval) : 0;
  }
  set tickInterval(value) {
    this._tickInterval = coerceNumberProperty(value);
  }
  private _tickInterval = 1;
}
