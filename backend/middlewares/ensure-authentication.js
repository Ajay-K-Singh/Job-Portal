const jwt = require('jsonwebtoken');
const jwtKey = require('../config/keys').jwtKey;
module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    jwt.verify(token, jwtKey.jwt);
    next();
  } catch(error) {
    res.status(401).json({
      message: 'Not Authorized'
    })
  }
};