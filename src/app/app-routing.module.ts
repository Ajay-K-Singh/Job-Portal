import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JobSeekerComponent } from './job-seeker/job-seeker.component';
import { RecruiterComponent } from './recruiter/recruiter.component';
import { PostJobsComponent } from './recruiter/post-jobs/post-jobs.component';
import { RecruiterLayoutComponent } from './layout/recruiter-layout/recruiter-layout.component';
import { JobSeekerLayoutComponent } from './layout/job-seeker-layout/job-seeker-layout.component';
import { JobSearchFormComponent } from './job-seeker/job-search-form/job-search-form.component';
import { JobListingsComponent } from './recruiter/job-listings/job-listings.component';
import { JobSeekerListingsComponent } from './job-seeker/jobseeker-listings/jobseeker-listings.component';
import { AuthorizationComponent } from './auth/auth.component';
import { AuthenticationGuard } from './services/auth.guard';
import { SeekerProfile } from './job-seeker/profile/seeker.profile';
import { RecruiterProfile } from './recruiter/profile/recruiter-profile.component';
import { ErrorComponent } from './error-route/error.component';
import { PageNotFoundComponent } from './error-route/page-not-found/page-not-found.component';

const routes: Routes = [
    {
        path: '',
        component:  AuthorizationComponent,
    },
    {
        path: 'job-seeker',
        component: JobSeekerLayoutComponent,
        canActivate: [AuthenticationGuard],
        children: [{
            path: '',
            component: JobSeekerListingsComponent,
            canActivate: [AuthenticationGuard]

        },
        {
          path: 'profile',
          component: SeekerProfile,
          canActivate: [AuthenticationGuard]
        }]
    },
    {
        path: 'recruiter',
        component: RecruiterLayoutComponent,
        canActivate: [AuthenticationGuard],
        children: [{
            path: '',
            component: JobListingsComponent,
            canActivate: [AuthenticationGuard]
        },
        {
            path: 'job-post/:id',
            component: PostJobsComponent,
            canActivate: [AuthenticationGuard]
        },
        {
          path: 'profile',
          component: RecruiterProfile,
          canActivate: [AuthenticationGuard]
        }
      ]
    },
    {
      path: 'unauthorized',
      component:  ErrorComponent,
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }

];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [AuthenticationGuard]
})
export class AppRoutingModule {

}
