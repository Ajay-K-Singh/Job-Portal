import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { JobPostingService } from '../../services/job-posting.service';
import { PostJob } from '../../models/post-job.model';
import { Subscription } from 'rxjs';
import { Router } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-job-listings',
  templateUrl: './job-listings.component.html',
  styleUrls: ['./job-listings.component.scss']
})
export class JobListingsComponent implements OnInit, OnDestroy {
  jobPosts: PostJob[] = [];
  private jobListings: Subscription;
  showJobPostComponent = false;

  constructor(public jobPostingService: JobPostingService, private router: Router) {}

  ngOnInit() {
    this.jobPostingService.getJobPosts();
    this.jobListings = this.jobPostingService.getJobPostsUpdateUpdateListener().subscribe((jobPosts: PostJob[]) => {
      this.jobPosts = jobPosts;
      console.log(jobPosts);
    });
  }

  onPostJob() {
    this.showJobPostComponent = true;
  }

  onDeleteJobPost(jobPostId: string) {
    this.jobPostingService.deleteJobPost(jobPostId);
  }

  onCancelJoPosting() {
    this.showJobPostComponent = false;
  }

  ngOnDestroy() {
    if (this.jobListings) {
      this.jobListings.unsubscribe();
     }
  }
}
