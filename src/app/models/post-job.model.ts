
export interface PostJob {
    id: string,
    jobTitle: string,
    nameOfCompany: object,
    location: string
}