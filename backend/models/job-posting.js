const mongoose = require('mongoose');

const jobPostSchema = mongoose.Schema({
    jobTitle: { type: String, required: true },
    nameOfCompany: { type: Object, required: true },
    experienceFrom: { type: Number},
    experienceTo: { type: Number},
    location: { type: String, required: true },
    keySkills: { type: Object },
    jobDescription: { type: String },
    recruiterInfo: String,
    salaryFrom: {type: Number},
    salaryTo: {type: Number}
});

module.exports = mongoose.model('JobPost', jobPostSchema);