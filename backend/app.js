const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require('cookie-parser');
const mongoose = require("mongoose");
const axios = require('axios');
const jobSeekerRoute = require("./routes/job-seeker");
const recruiterRoute = require("./routes/recruiter");
const passport = require('passport');
const socialAuth = require('./routes/social-auth');
const session = require("express-session");
const cors = require('cors');
const uuid = require('uuid/v4')
const app = express();

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
app.use(passport.initialize());

require('./passport-strategies/google')(passport);
require('./passport-strategies/facebook')(passport);
require('./passport-strategies/linkedin')(passport);
require('./passport-strategies/github')(passport);

mongoose
  .connect(
    'mongodb+srv://ajay:ajay1493@job-portal-ziech.mongodb.net/job-portal?retryWrites=true', { useNewUrlParser: true }
  )
  .then(() => {
    console.log("Connected to database!");
  })
  .catch(() => {
    console.log("Connection failed!");
  }); 


app.use(cors({origin: ["http://localhost:4200"], credentials: true}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
  genid: (req) => {
    return uuid()
  },
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}));

app.get("/api/google-places", (req, res) => {
  const key = 'AIzaSyChQkfsc-eJpBi4L_g3QVl66TV9clsMRWg';
  const url = `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${req.query.location}&types=(cities)&language=pt_BR&key=${key}`
  axios.get(url).then(response => res.send(response.data)).catch(err => console.log(err));
});

app.get("/api/suggest-companies", (req, res) => {
	const url = `https://autocomplete.clearbit.com/v1/companies/suggest?query=${req.query.nameOfCompany}`;
	axios.get(url).then(response => res.send(response.data)).catch(err => console.log(err));
});

app.get("/api/suggest-skills", (req, res) => {
  const url = `http://api.dataatwork.org/v1/skills/autocomplete?begins_with=${req.query.skill}`;
  axios.get(url).then(response => res.send(response.data)).catch(err => console.log(err));
});

app.get("/api/validate-session", (req, res) => {
  console.log('Hello');
  if (req.session.isAuthenticated) {
    const now = new Date().getTime();
    const loggedInTime = new Date(Date.parse(req.session.loggedInTime));
    req.session.expiresIn = ((loggedInTime.getTime() + (1 * 60 * 60 * 1000))  -  now)/1000;
  }
  if (req.session.isAuthenticated && req.session.expiresIn > 0) {
    res.status(200).json({
      user: req.session
    });
  } else {
    req.session.isAuthenticated = false;
    req.session.userInfo = null;
    req.session.expiresIn = null;
    req.session.loggedInTime = null;
    res.status(200).json({
      message: 'Logged Out',
      user: req.session
    });
  }
});

app.get('/logout', (req, res) => {
  req.session.isAuthenticated = false;
  req.session.userInfo = null;
  req.session.expiresIn = null;
  req.session.loggedInTime = null;
  req.session.destroy((err) => {
    if (err) {
      res.status(500).json({
        message: 'Could Not Log Out.'
      })
    } else {
      res.status(200).json({
        message: 'Logged Out'
      })
    }
  })
  
});

app.use("/auth", socialAuth);
app.use("/api/job-seeker", jobSeekerRoute);
app.use("/api/recruiter", recruiterRoute);


module.exports = app;