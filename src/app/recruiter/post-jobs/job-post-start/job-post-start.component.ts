import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { JobPostingService } from '../../../services/job-posting.service';
import { Subscription } from 'rxjs';
import { Validators } from '@angular/forms';
import { FormGroup, FormControl } from '@angular/forms';
import { CustomErrorStateMatcher } from '../../../material/error-state-matcher';

@Component({
  selector: 'app-post-job-start',
  templateUrl: './job-post-start.component.html',
  styleUrls: ['./job-post-start.component.scss']
})
export class PostJobStartComponent implements OnInit {
  jobPostStartForm: FormGroup;
  @Input() showJobPostComponent: Boolean;
  @Output()
  private _onCancelStartJobPost: EventEmitter<any> = new EventEmitter<any>();
  public get onCancelStartJobPost(): EventEmitter<any> {
    return this._onCancelStartJobPost;
  }
  public set onCancelStartJobPost(value: EventEmitter<any>) {
    this._onCancelStartJobPost = value;
  }
  matcher = new CustomErrorStateMatcher();
  locationLabel = 'Location';
  locationPlaceholder = 'Location of the Job';
  nameOfCompanyLabel = 'Name Of Copmany';
  companyInfo: object;
  skillSet = [];
  constructor(public jobPostingService: JobPostingService) { }

  ngOnInit() {
    this.jobPostStartForm = new FormGroup({
      'jobTitle': new FormControl(null, Validators.required)
    });
  }


  private addFormControl(name: string, formGroup: FormGroup): void {
    this.jobPostStartForm.addControl(name, formGroup);
  }

  onSubmit() {
    this.jobPostingService.startJobPosting(
      this.jobPostStartForm.value.jobTitle, this.companyInfo,
      this.jobPostStartForm.value.location['location']);
  }

  onCancelJobPosting() {
    this.onCancelStartJobPost.emit();
  }

  private setSelectedCompamy(event): void {
    this.companyInfo = {
      nameOfCompany: event.company.name,
      logo: event.company.logo,
      domain: event.company.domain
    };
  }


}
